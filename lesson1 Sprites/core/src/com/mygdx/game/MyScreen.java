package com.mygdx.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by TRBLG on 30.01.2017.
 */

public class MyScreen implements Screen {
    SpriteBatch batch;
    Texture img; //чаще всего статичные изображения. Например фон. Нельзя изменять, но можно накладывать фильтры
    private Game game;
    TextureRegion region;
    Sprite sprite;

    @Override
    public void show() {
        batch = new SpriteBatch();
        img = new Texture("badlogic.jpg");
        //img.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Nearest);   //задаем фильтр текстуре

        region = new TextureRegion(img, 50, 30, 100, 120);  // вырезаем регион из текстуры img начиная с координат х(50)
                                                            // и y(30) на n(100) пикселей по х и на m(120) пикселей по y

        //region.setRegion(50, 30, 100, 120); // задаем новые координаты и размеры имеющемуся региону

        sprite = new Sprite(region); // инициализируем спрайт. передаем в качестве параметра текстуру или ее регион
        sprite.setBounds(100, 100, 100, 130); //задать позицию и размеры спрайта
        //sprite.setSize(50, 60); // задать размеры
        //sprite.setPosition(60, 40); // задать коодинаты
        //sprite.setScale(2, 4); //увеличить по х и по у
        //sprite.setRotation(32); //повернуть на n градусов
        //sprite.setFlip(false, true); // отзеркалить по y и не отзеркаливать по x



    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.begin();
        //batch.draw(img, 0, 0);  //отрисовываем текстуру(текстура, координата по х, коордиената по y)
        sprite.draw(batch); // отрисовываем спрайт
        batch.end();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
