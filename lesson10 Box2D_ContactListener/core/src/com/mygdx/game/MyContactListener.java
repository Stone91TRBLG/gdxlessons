package com.mygdx.game;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.World;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by TRBLG on 11.02.2017.
 */

public class MyContactListener implements ContactListener {
    World world;
    boolean flag = false;
    List<Body> bodyList = new ArrayList<Body>();

    public MyContactListener(World world){
        this.world = world;
    }


    @Override
    public void beginContact(Contact contact) {
        //Срабатывает, когда два объекта начинают накладываться. Прокает только в рамках шага.


        contact.getFixtureA();//Получаем фикстуру первого объекта
        contact.getFixtureB();//Получаем фикстуру второго объекта
        contact.getFixtureA().getUserData();// получаем имя объекта. так можем его идентифицировать

    }

    @Override
    public void endContact(Contact contact) {
        //Срабатывает, когда два объекта прекращают соприкасаться. Может быть вызван,
        // когда тело разрушено, таким образом, это событие может иметь место вне временного шага.

        System.out.println(contact.getFixtureA().getUserData());
        System.out.println(contact.getFixtureB().getUserData());


    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {
        //Срабатывает после обнаружения столкновения, но перед его обработкой.
        // Это позволяет нам как-то изменить контакт до его обработки. Например, можно сделать контакт неактивным
        //Например можно пройти через тело(платформу снизу)


    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {
        //Метод позволяет осуществить логику игры, которая изменяет физику после контакта.
        // Например, деформировать или уничтожить объект после контакта. Однако, Box2D не позволяет вам изменять физику в методе,
        // потому что вы могли бы разрушить объекты, которые Box2D в настоящее время обрабатывает, приводя к ошибке.


        // Удалять сразу нельзя. Это почти наверняка вызровет ошибку
        checkBody(contact);



    }

    public void checkBody(Contact contact){
        if((contact.getFixtureA() != null) && (contact.getFixtureA().getUserData() == null) ){
            //проверяем UserData(посути имя) объекта
            bodyList.add(contact.getFixtureA().getBody());// добавляем объект в список объектов на удаление

            flag = true; // выставляем флаг, говорящий о том, что список объектов на удаление не пуст
        }

        if((contact.getFixtureB() != null) && (contact.getFixtureB().getUserData() == null) ){
            bodyList.add(contact.getFixtureB().getBody());
            flag = true;
        }

    }

    public void destroyBody(){

        for (int i = 0; i < bodyList.size(); i++) {
            //bodyList.get(0).setActive(false);
            world.destroyBody(bodyList.get(0));//поочередно удаляем все объекты в списке
        }
        bodyList.clear();// очищаем сисок
    }


    public boolean getFlag(){
        return flag;
    }
}
