package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

/**
 * Created by TRBLG on 15.02.2017.
 */

public class MyActor extends Actor {
    private Sprite sprite;

    @Override
    public void draw(Batch batch, float parentAlpha) {
        sprite.draw(batch);
    }

    public MyActor(int y){
        sprite = new Sprite(new Texture("badlogic.jpg"));
        sprite.setBounds(getX(), getY(), getWidth(), getHeight());
        setBounds(5, y, 8, 2);

        addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                System.out.println("кнопка нажата");
                super.clicked(event, x, y);
            }
        });


    }


    @Override
    public void act(float delta) {
        sprite.setBounds(getX(), getY(), getWidth(), getHeight());
        super.act(delta);
    }
}
