package com.mygdx.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.viewport.FillViewport;

/**
 * Created by TRBLG on 15.02.2017.
 */

public class MyScreen implements Screen {
    Game game;
    Stage stage;

    MyActor actor;
    OrthographicCamera camera;

    float aspectRatio;

    public MyScreen(Game game){
        this.game = game;
    }


    @Override
    public void show() {

        aspectRatio = (float) Gdx.graphics.getWidth() / Gdx.graphics.getHeight();

        camera = new OrthographicCamera(20, 20/aspectRatio);
        camera.position.set(new Vector3(10, (20/aspectRatio)/2, 0 ));

        stage = new Stage(new FillViewport(20, 20 / aspectRatio, camera));

        actor = new MyActor(3);
        stage.addActor(new MyActor(3));
        stage.addActor(new MyActor(6));
        stage.addActor(new MyActor(9));

        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        aspectRatio = width/height;

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
