package com.mygdx.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector3;

/**
 * Created by TRBLG on 30.01.2017.
 */

public class MyScreen implements Screen {
    SpriteBatch batch;
    Texture img;
    private Game game;
    Sprite sprite;
    OrthographicCamera camera;
    int screenWidth, screenHeight;

    int x,y;

    public MyScreen(Game game){
        this.game = game;
    }

    @Override
    public void show() {
        batch = new SpriteBatch();
        img = new Texture("badlogic.jpg");
        screenWidth = Gdx.graphics.getWidth(); //получаем ширину экрана устройства или окна
        screenHeight = Gdx.graphics.getHeight(); // получем высоту экрана устройства или окна
        x = screenWidth/2;
        y = screenHeight/2;


        camera = new OrthographicCamera(screenWidth, screenHeight); //задаем камеру с параметрами ширины и высоты.

        camera.position.set(new Vector3(x, y, 0));

        sprite = new Sprite(img);
        sprite.setBounds(100, 100, 150, 150);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // сначала выполняются все манипуляции с камерой(позиция, угол и т.п.)
        // затем елается обновление камеры(camera.update)
        // и только после этого задаем матрицу для отображения(посути отрисовываем изображение)


        cameraControll(); //используем метод управления камерой, описанный в самом низу класса

        camera.position.x = x;  //данной командой, манипулируя переменными двигаем камеру по х.
                                                    // например берем координаты курсора.
        camera.position.y = y;  // Работа с у аналгична

        camera.update();
        batch.setProjectionMatrix(camera.combined); //Задаем матрицу для камеры. Перед этим обязателен update камеры

        batch.begin();
        batch.draw(img,0,0);
        batch.end();
    }


    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

    public void cameraControll(){

        //управляем камерой при помощи клавиатуры
        if(Gdx.input.isKeyPressed(Input.Keys.A)){
            x--;
        }
        if(Gdx.input.isKeyPressed(Input.Keys.D)){
            x++;
        }
        if(Gdx.input.isKeyPressed(Input.Keys.S)){
            y--;
        }
        if(Gdx.input.isKeyPressed(Input.Keys.W)){
            y++;
        }
    }
}
