package com.mygdx.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;

/**
 * Created by TRBLG on 30.01.2017.
 */

public class MyScreen implements Screen {
    SpriteBatch batch;
    Texture img;
    private Game game;
    OrthographicCamera camera;
    int screenWidth, screenHeight;
    float aspectRatio;
    int sizeSide = 20; // задаем размер сторон(обязательно должен быть квадрат. так проще работать)

    float x,y;

    public MyScreen(Game game){
        this.game = game;
    }

    @Override
    public void show() {
        batch = new SpriteBatch();
        img = new Texture("badlogic.jpg");

        screenWidth = Gdx.graphics.getWidth(); //получаем ширину экрана устройства или окна
        screenHeight = Gdx.graphics.getHeight(); // получем высоту экрана устройства или окна

        aspectRatio = (float)screenWidth/screenHeight;
        x = sizeSide/2;
        y = (sizeSide/aspectRatio)/2;


        camera = new OrthographicCamera(x*2, y*2); //тут лучше задавать "метры". Проще работать с box2d
        camera.position.set(new Vector3(x, y, 0));
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // сначала выполняются все манипуляции с камерой(позиция, угол и т.п.)
        // затем елается обновление камеры(camera.update)
        // и только после этого задаем матрицу для отображения(посути отрисовываем изображение)


        cameraControll(); //используем метод управления камерой, описанный в самом низу класса

        camera.position.x = x;  //данной командой, манипулируя переменными двигаем камеру по х.
        // например берем координаты курсора.
        camera.position.y = y;  // Работа с у аналгична

        camera.update();
        batch.setProjectionMatrix(camera.combined); //Задаем матрицу для камеры. Перед этим обязателен update камеры

        batch.begin();
        batch.draw(img,0, 0, sizeSide, sizeSide);
        batch.end();
    }


    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

    public void cameraControll(){

        //управляем камерой при помощи клавиатуры
        if(Gdx.input.isKeyPressed(Input.Keys.A)){
            x = x - 0.1f;
        }
        if(Gdx.input.isKeyPressed(Input.Keys.D)){
            x = x + 0.1f;
        }
        if(Gdx.input.isKeyPressed(Input.Keys.S)){
            y = y - 0.1f;
        }
        if(Gdx.input.isKeyPressed(Input.Keys.W)){
            y = y + 0.1f;
        }
    }
}

