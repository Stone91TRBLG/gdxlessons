package com.mygdx.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;

/**
 * Created by TRBLG on 31.01.2017.
 */

public class MyScreen implements Screen {
    SpriteBatch batch;
    Texture forest, grass;
    private Game game;
    OrthographicCamera camera;
    int screenWidth, screenHeight;
    float aspectRatio;
    int sizeSide = 24;
    float x,y;
    float paralaxForestX1 = 0, paralaxForestX2 = 1;
    float paralaxGrassX1 = 0, paralaxGrassX2 = 1;

    public MyScreen(Game game){
        this.game = game;
    }

    @Override
    public void show() {
        batch = new SpriteBatch();
        forest = new Texture("forest.png");
        grass = new Texture("big_grass.png");

        screenWidth = Gdx.graphics.getWidth();
        screenHeight = Gdx.graphics.getHeight();

        aspectRatio = (float)screenWidth/screenHeight;
        x = sizeSide/2;
        y = (sizeSide/aspectRatio)/2;


        camera = new OrthographicCamera(x*2, y*2);
        camera.position.set(new Vector3(x, y, 0));
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        cameraControll();

        camera.position.x = x;

        camera.position.y = y;

        camera.update();
        batch.setProjectionMatrix(camera.combined);
        batch.begin();

        paralaxForest();
        paralaxGrass();


        batch.end();
    }


    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

    public void cameraControll(){

        if(Gdx.input.isKeyPressed(Input.Keys.A)){
            x = x - 0.1f;
        }
        if(Gdx.input.isKeyPressed(Input.Keys.D)){
            x = x + 0.1f;
        }
        if(Gdx.input.isKeyPressed(Input.Keys.S)){
            y = y - 0.1f;
        }
        if(Gdx.input.isKeyPressed(Input.Keys.W)){
            y = y + 0.1f;
        }
        if(Gdx.input.isKeyPressed(Input.Keys.ESCAPE)){

        }
    }

    public void paralaxForest(){
        float a, b;
        a = (paralaxForestX1 * sizeSide*1.5f) + (x / 10);
        b = (paralaxForestX2 * sizeSide*1.5f) + (x / 10);


        if( a < x - sizeSide * 1.5f * 1.5f ){
            paralaxForestX1 = paralaxForestX1 + 2;
        }

        if( b < x - sizeSide * 1.5f * 1.5f  ){
            paralaxForestX2 = paralaxForestX2 + 2;
        }

        batch.draw(forest, a, 2.5f, sizeSide*1.5f, sizeSide/3.5f);
        batch.draw(forest, b, 2.5f, sizeSide*1.5f, sizeSide/3.5f);


    }

    public void paralaxGrass(){
        float a, b;
        a = paralaxGrassX1 * sizeSide * 1.1f;
        b = paralaxGrassX2 * sizeSide * 1.1f;


        if(a < x - sizeSide * 1.1f  * 1.5f ){
            paralaxGrassX1 = paralaxGrassX1 + 2;
        }

        if(b < x - sizeSide * 1.1f * 1.5f ){
            paralaxGrassX2 = paralaxGrassX2 + 2;
        }

        batch.draw(grass, a, 1.95f, sizeSide*1.1f, sizeSide/12);
        batch.draw(grass, b, 1.95f, sizeSide*1.1f, sizeSide/12);
    }


}


