package com.mygdx.game;

import com.badlogic.gdx.InputProcessor;

/**
 * Created by TRBLG on 31.01.2017.
 */

public class MyInputController implements InputProcessor {
    CursorCoordinatSendler cursor;

    public MyInputController(CursorCoordinatSendler cursor){
        this.cursor = cursor;
    }


    @Override
    public boolean keyDown(int keycode) {
        //возвращает нажатую кнопку
        System.out.println("keyDown " + keycode);
        cursor.getButtonCode(keycode);
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        //возвращает отпущеную кнопку
        System.out.println("keyUp " + keycode);

        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        //возвращает символ склавы
        System.out.println("keyTyped " + character);

        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        //возвращает координаты клика при нажатии
        //pointer - разные типы(одно косание, два косания ..)
        System.out.println("touchDown X:" + screenX + " Y: " + screenY + " pointer: " + pointer + " button: " + button);

        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        //возвращает координаты клика при отпускании
        //pointer - разные типы(одно косание, два косания ..)
        System.out.println("touchUp X:" + screenX + " Y: " + screenY + " pointer: " + pointer + " button: " + button);

        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        //всегда выводит координаты нажатия тача
        System.out.println("touchDragged X:" + screenX + " Y: " + screenY + " pointer: " + pointer);

        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        //всегда выводит координаты
        System.out.println("mouseMoved X:" + screenX + " Y: " + screenY);

        cursor.getCursorPosition(screenX, screenY);
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        //скролл
        System.out.println("scrolled " + amount);

        return false;
    }
}
