package com.mygdx.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

import box2dLight.ConeLight;
import box2dLight.DirectionalLight;
import box2dLight.PointLight;
import box2dLight.RayHandler;

/**
 * Created by TRBLG on 02.02.2017.
 */

public class MyScreen implements Screen {
    Game game;
    World world;
    Box2DDebugRenderer rend;
    RayHandler handler;
    OrthographicCamera camera;
    int screenWidth, screenHeight;
    float aspectRatio;
    int sizeSide = 20;
    float x, y, lightX, lightY;
    CursorCoordinatSendler cursor;
    PointLight point;// источник света "точка". свет  во все стороны
    ConeLight coneLight; // направленный источник света как у фонаря

    Body body;

    MyInputController controller;

    public MyScreen(Game game){
        this.game = game;
    }

    @Override
    public void show() {

        screenWidth = Gdx.graphics.getWidth();
        screenHeight = Gdx.graphics.getHeight();
        aspectRatio = (float) screenWidth / screenHeight;
        world = new World(new Vector2(1, 0), true);
        x = sizeSide / 2;
        y = (sizeSide / aspectRatio) / 2;
        camera = new OrthographicCamera(x * 2, y * 2);
        camera.position.set(new Vector3(x, y, 0));
        rend = new Box2DDebugRenderer();



        handler = new RayHandler(world);   //hendler света
        point = new PointLight(handler, 200, Color.YELLOW, 20, 5, 5);// слушатель,
                                                                // количество лучей(чем больше тем качественнее и плавнее,
                                                                //  но ест ресурсы), цвет, радиус, х, у
        point.setSoft(true);//смягчает тени. Не сильно влияет

        coneLight = new ConeLight(handler, 150, Color.PURPLE, 20, 15, 15, -90, 29); // создаем источник в виде направленного конуса



        handler.setAmbientLight(Color.BLUE);//задаем цвет. нельзя ставить после устанавливания затемнения(следующего метода)
        handler.setAmbientLight(0.3f);// задаем затемнение(область вне источника света) 1- светлое, 0 темное
       // handler.setAmbientLight(0.3f, 0.3f, 0.7f, 0.1f);
        handler.setBlur(true);//делает свет плавнее, но занимает ресурсы
        handler.setBlurNum(5);//задает колличетсво итераций блюра. делает свет(точнее просчет теней) еще приятнее
        handler.setGammaCorrection(true);// делает свет еще более реалистичным




    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        cursor = new CursorCoordinatSendler() {
            @Override
            public void getCursorPosition(int x, int y) {
                //получаем координаты курсора и присваиваем их источнику света
                lightX = (float)x/((float)screenWidth/20.0f);
                lightY = (float) (screenHeight - y) / ((float)screenHeight/((float) 20/ aspectRatio));
                point.setPosition(lightX, lightY);
            }

            @Override
            public void getButtonCode(int code) {
                if(code == 62){
                    createNewBody(lightX, lightY, 0);
                }
                if(code == 59){
                    createNewBody(lightX, lightY, 1);
                }
                if(code == 112){
                    //delete
                }
            }
        };
        controller = new MyInputController(cursor);
        Gdx.input.setInputProcessor(controller);


        handler.setCombinedMatrix(camera);
        handler.updateAndRender();
        rend.render(world, camera.combined);
        world.step(1/60f, 4, 4);

        camera.update();

    }

    @Override
    public void resize(int width, int height) {
        System.out.println("resize");
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

    public void createNewBody(Float x, float y, int type){
        //метод для создания объектов Box2d
        BodyDef bDef = new BodyDef();
        bDef.type = BodyDef.BodyType.DynamicBody;
        bDef.position.set(x, y);

        body = world.createBody(bDef);

        FixtureDef fDef = new FixtureDef();


        PolygonShape shape  = new PolygonShape();
        shape.setAsBox(0.5f, 0.5f);

        CircleShape cShape = new CircleShape();
        cShape.setRadius(0.5f);

        if(type == 0) {
            fDef.shape = shape;
            fDef.density = 2;

            body.createFixture(fDef);
        }
        if(type == 1){
            PointLight l;
            l = new PointLight(handler, 200, Color.YELLOW, 5.5f, 5, 5);
            fDef.shape = cShape;
            fDef.density = 2;

            body.createFixture(fDef);
            l.attachToBody(body);//прикрепляем к телу свет

        }

    }
}
