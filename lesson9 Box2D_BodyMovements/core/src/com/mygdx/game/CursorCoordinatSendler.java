package com.mygdx.game;

/**
 * Created by TRBLG on 02.02.2017.
 */

public interface CursorCoordinatSendler {
    void getCursorPosition(int x, int y);
    void getButtonCode(int code);

}
