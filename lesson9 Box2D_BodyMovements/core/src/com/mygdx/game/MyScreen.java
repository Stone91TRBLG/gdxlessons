package com.mygdx.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.sun.scenario.effect.light.Light;


/**
 * Created by TRBLG on 02.02.2017.
 */

public class MyScreen implements Screen {
    Game game;
    World world;
    Box2DDebugRenderer rend;

    OrthographicCamera camera;
    int screenWidth, screenHeight;
    float aspectRatio;
    int sizeSide = 20;
    float x, y, lightX, lightY;
    CursorCoordinatSendler cursor;
    float pixelsInX, pixelsInY;
    boolean barier = false;

    Body body;
    Vector2 v;

    MyInputController controller;

    public MyScreen(Game game){
        this.game = game;
    }

    @Override
    public void show() {
        System.out.println("show");

        v = new Vector2(0, 0);
        world = new World(v, true);

        x = sizeSide / 2;
        y = (sizeSide / aspectRatio) / 2;
        //camera = new OrthographicCamera(x * 2, y * 2);
        camera = new OrthographicCamera(0, 0);
        //camera.position.set(new Vector3(x, y, 0));
        rend = new Box2DDebugRenderer();

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        createBarier();


        cursor = new CursorCoordinatSendler() {
            @Override
            public void getCursorPosition(int x, int y) {
                //получаем координаты курсора и присваиваем их источнику света
                lightX = (float) (x/pixelsInX);
                lightY = (float) ((screenHeight - y)/pixelsInY);

            }

            @Override
            public void getButtonCode(int code) {
               switch (code){
                   case 62:
                       //space
                      // body.applyForceToCenter(new Vector2(500, 500), true);// оказываем импульс(толчек) на тело с помощью вектора
                       body.setLinearVelocity(new Vector2(1, 0));//задаем вектор движения не импульсный, а постояное движение

                       break;


                   case 29:
                       //A
                       body.applyForceToCenter(new Vector2(-500, 0),true);
                       break;
                   case 32:
                       //D
                       body.applyForceToCenter(new Vector2(500, 0),true);
                       break;
                   case 47:
                       //S
                       body.applyForceToCenter(new Vector2(0, -500),true);
                       break;
                   case 51:
                       //w
                       body.applyForceToCenter(new Vector2(0, 500),true);
                       break;
                   default:
                       break;
               }
            }
        };
        controller = new MyInputController(cursor);
        Gdx.input.setInputProcessor(controller);

        rend.render(world, camera.combined);
        world.step(1/60f, 4, 4);

        camera.update();

    }

    @Override
    public void resize(int width, int height) {
        System.out.println("resize");
        screenWidth = width;
        screenHeight = height;
        aspectRatio = (float) width / height;

        pixelsInX = (float) width/20f;
        pixelsInY = (float)height/(20f/aspectRatio);

        x = sizeSide / 2;
        y = (sizeSide / aspectRatio) / 2;

        camera = new OrthographicCamera(x * 2, y * 2);

        camera.viewportWidth = x*2;
        camera.viewportHeight = y*2;
        camera.position.set(new Vector3(x, y, 0));
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

    public void createNewBody(float x, float y){
        //метод для создания объектов Box2d
        System.out.println("createNewBody");
        BodyDef bDef = new BodyDef();
        bDef.type = BodyDef.BodyType.DynamicBody;
        bDef.position.set(x, y);

        body = world.createBody(bDef);
        FixtureDef fDef = new FixtureDef();

        CircleShape shape = new CircleShape();
        shape.setRadius(0.5f);
        fDef.shape = shape;

        //fDef.density = 2;
        fDef.friction = 0;
        fDef.restitution = 1;
        body.createFixture(fDef);
    }

    public void createBarier(){
        if(!barier) {
            BodyDef bDef = new BodyDef();
            bDef.type = BodyDef.BodyType.StaticBody;
            //bDef.position.set(x, y);

            Body bod;
            bod = world.createBody(bDef);
            FixtureDef fDef = new FixtureDef();

            ChainShape shape = new ChainShape();
            shape.createChain(new Vector2[]{new Vector2(0.1f, 0.1f), new Vector2(19.9f, 0.1f), new Vector2(19.9f, (float) 20 / aspectRatio - 0.1f), new Vector2(0.1f, (float) 20 / aspectRatio - 0.1f), new Vector2(0.1f, 0.1f)});

            fDef.shape = shape;

            fDef.density = 2;
            fDef.friction = 0;
            bod.createFixture(fDef);
            barier = true;

            createNewBody(10,10);
        }

    }
}
